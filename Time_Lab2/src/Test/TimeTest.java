package Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import time.Time;

/**
 * 
 * @author Gaurav
 *
 */

public class TimeTest {
	
	@Test
	public void testGetTotalMilliscondsBoundaryIn() {
		int totalMilliseconds = Time.getTotalMilliseconds( "12:05:50:999" );
		assertTrue("Invalid Number of MilliSeconds" , totalMilliseconds == 999);
		//fail("Invalid Number of milliseconds");
	}
	
	@Test (expected = NumberFormatException.class)
	public void testGetTotalMilliscondsBoundaryOut() {
		int totalMilliseconds = Time.getTotalMilliseconds( "12:05:50:1000" );
		//assertTrue("Invalid Number of MilliSeconds" , totalMilliseconds != 1000);
		fail("Invalid Number of milliseconds");
	}

	@Test
	public void testGetTotalMilliscondsRegular() {
		int totalMilliseconds = Time.getTotalMilliseconds( "12:05:05:05" );
		assertTrue("Invalid Number of MilliSeconds" , totalMilliseconds == 5);
	}
	
	@Test (expected = NumberFormatException.class)
	public void testGetTotalMillisecondsException() 
	{
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:0A");
		fail ("Invalid number of milliseconds");
	}
	
	
}
